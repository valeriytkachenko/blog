<?php

use yii\db\Migration;

class m171011_005914_add_date_to_comment extends Migration
{
    public function safeUp()
    {
        $this->addColumn('comment', 'date', $this->timestamp()->notNull());
    }

    public function safeDown()
    {
        echo "m171011_005914_add_date_to_comment cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171011_005914_add_date_to_comment cannot be reverted.\n";

        return false;
    }
    */
}
