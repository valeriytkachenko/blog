<?php
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
?>
<!--comments section-->
<div class="row">

    <div class="col-md-8 col-md-offset-2">
        <div class="page-header">
            <h3><small class="pull-right"><?=count($article->getAllowedComments())?> comments</small> Comments </h3>
        </div> 
        
        <div class="comments-list">
            <?php if(!empty($comments)):?>

                <!-- alert that comment will appear soon-->
                <div class="alert-message">
                    <?php if(Yii::$app->session->getFlash('comment')):?>
                        <div class="alert alert-success" role="alert">
                            <?= Yii::$app->session->getFlash('comment'); ?>
                        </div>
                    <?php endif;?>
                </div>
                <!--end of allert block-->

                <!-- show tree of allowed comments-->
                <?php foreach($comments as $comment){
                    echo '<ul>';
                    outTree($comment, $maxLevel);
                    echo '</ul>';
                    }
                ?>
                <!-- end of allowed comments tree-->

            <?php else: ?>
                    <h4 class="text-center">There are no comments yet</h4>
            <?php endif; ?>

            <?php if(!\Yii::$app->user->isGuest):?>
                <p><a role='button' class='reply' href='#'>Leave a comment</a></p>
            <?php else:?> 
                <a href="<?=\Yii::$app->request->BaseUrl.'/site/login'?>">Login to leave a comment</a>
            <?php endif?>

            <?php function outTree($comment, $maxLevel, $level=0) {?>
                    <li>
                        <p class='pull-right'><small><?=$comment->date?></small></p>
                        <div class='media-body'>
                            <h4 class='media-heading user_name'><b><?=$comment->user->username?></b></h4>
                            <?=$comment->text?>
                            <?php if(!\Yii::$app->user->isGuest && $level != $maxLevel):?>
                            
                                <p><small><a role='button' class='reply' comment-id = '<?=$comment->id?>'href='#'>Reply</a></small></p>
                            <?php endif;?>
                            <hr>
                        </div>
                    </li>
                <?php
                    if($comment->child)
                    {   $level++;
                        echo '<ul>';
                        foreach($comment->child as $child)
                        {
                            outTree($child, $maxLevel, $level);
                        }
                        echo '</ul>';
                    }
                }
                ?>

                <!-- leaving message form -->
                <div style="display: none;" class="">     
                    <?php $form = \yii\widgets\ActiveForm::begin([
                        'action' => ['site/comment', 'id' => $article->id],
                        'options' => ['class'=>'form-horizontal comment-form', 'role'=>'form']])?>

                    <div class="row">
                        <div class="">
                            <?= $form->field($commentForm, 'parent_id')->hiddenInput(['value' => 0, 'id' => 'parent_id_input'])->label(false);?>
                            <?= $form->field($commentForm, 'comment')->textarea(['class'=>'form-control','placeholder'=>'Write Message'])->label(false)?>
                        </div>
                        <button type="submit" class="btn send-btn pull-right comment-btn">Post Comment</button>
                    </div>
                    <?php \yii\widgets\ActiveForm::end();?>
                </div>
                <!-- end of leaving message form -->

        </div>
    </div>  
</div> <!--comments section-->