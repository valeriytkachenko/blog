<?php
namespace app\widgets;
use yii\base\Widget;

/**
* Comments tree widget
*/
class CommentsWidget extends Widget
{
    public $comments;
    public $article;
    public $commentForm;
    public $maxLevel = 2; //starts from 0
    
    function run()
    {   
        return $this->render(
            'comments',
        [
            'comments' => $this->comments,
            'article' => $this->article,
            'commentForm' => $this->commentForm,
            'maxLevel' => $this->maxLevel,
        ]);
    }
}