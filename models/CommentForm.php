<?php

namespace app\models;

use yii\base\Model;
use Yii;

class CommentForm extends Model
{
    public $comment;
    public $parent_id;

    public function rules()
    {
        return [
            [['comment'], 'required'],
            [['parent_id'], 'integer'],
            [['comment'], 'string', 'length' => [3,250]]
        ];
    }
    /**
    * Saves comment
    * @param int $article_id
    * @return bool
    */
    public function saveComment($article_id)
    {   
        $comment = new Comment;
        $comment->text = $this->comment;
        $comment->parent_id = $this->parent_id;
        $comment->user_id = Yii::$app->user->id;
        $comment->article_id = $article_id;
        $comment->status = 0;

        return $comment->save();   
    }

}