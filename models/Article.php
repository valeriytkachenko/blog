<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "article".
 *
 * @property integer $id
 * @property string $title
 * @property string $description
 * @property string $content
 * @property string $date
 * @property string $image
 */
class Article extends \yii\db\ActiveRecord
{   
	public $picture;
	public $filename;
	public $string;

	public static function tableName()
	{
		return 'article';
	}

	/**
     * @return array of validation rules.
     */
	public function rules()
	{
		return [
			[['description', 'content'], 'string'],
			[['date'], 'safe'],
			[['description'], 'string', 'max' => 250],
			[['title'], 'string',  'max' => 100],
		];
	}

	 /**
     * @return array of attribute labels.
     */
	public function attributeLabels()
	{
		return [
			'id' => 'ID',
			'title' => 'Title',
			'description' => 'Description',
			'content' => 'Content',
			'date' => 'Date',
			'image' => 'Image',
		];
	}

 	/**
     * Uploads image to server before saving article
     */
	public function beforeSave($insert)
	{
		//if record is new
		if ($this->isNewRecord)
		{
			//creating unique name
			$this->string = substr(uniqid('img'), 0, 12); //example: img01230120
			$this->picture = UploadedFile::getInstance($this, 'image');

			//adding file extension to name
			$this->filename = $this->string . '.' . $this->picture->extension;
			
			//saving
			$this->picture->saveAs('uploads/images/' . $this->filename);

			$this->image = $this->filename;
		}
		//if updating article
		else
		{
			//update post
			$this->picture = UploadedFile::getInstance($this, 'image');
			if($this->picture)
			{	
				//creating unique name
				$this->string = substr(uniqid('img'), 0,12);

				//adding file extension to name
				$this->filename = $this->string . '.' .$this->picture->extension;

				$this->image = $this->filename;
				$this->picture->saveAs('uploads/images/' . $this->filename);
			}
		}
		
		return parent::beforeSave($insert);
	}
	
	public function getComments()
	{
		return $this->hasMany(Comment::class, ['article_id' => 'id']);	
	}

	public function getAllowedComments()
	{
		return $this->getComments()->where(['status'=>1])->all();
	}
	/**
     * Recursive function 
	 * builds tree of comments
	 * @return array $tree of comments.
     */
	public function commentsTree(&$data, $root = 0)
	{
        $tree = array();
        foreach ($data as $id => $node)
		{
            if ($node->parent_id == $root)
			{
                $node->child = $this->commentsTree($data, $node->id);
                $tree[] = $node;
            }
        }
        return $tree;
    }
}