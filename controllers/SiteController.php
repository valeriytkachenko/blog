<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\User;
use app\models\SignupForm;
use app\models\ContactForm;
use app\models\Comments;
use app\models\CommentForm;
use app\models\Article;
use yii\data\Pagination;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /**
    * Display all articles by pages
    */
    public function actionIndex()
    {
        $query = Article::find();
        $pages = new Pagination(['totalCount' => $query->count(),'pageSize' => 3]);
        $articles = $query->offset($pages->offset)
        ->limit($pages->limit)
        ->orderBy('id desc') 
        ->all();

        return $this->render('index', ['articles' => $articles,  'pages' => $pages]);
    }
    /**
    * Display one article
    */
    public function actionArticle($id)
    {
        //if exists
        if($article = Article::find()->Where(['id' => $id])->one())
        {   
            $comments = $article->getAllowedComments(); 
            $comments = $article->commentsTree($comments);
            
            $commentForm = new CommentForm();
            return $this->render('article', ['article' => $article, 
                                             'comments' => $comments,
                                             'commentForm' => $commentForm,
                                             ]);
        }
        else
        {
            throw new NotFoundHttpException('Такой записи не существует!');
        }

    }
    /**
    * Authorization
    */
    public function actionLogin()
    {   
        //If user logged - redirect to homepage
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        //Else log in user
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
    * Registration
    */
    public function actionSignup()
    {   
        //If user logged - redirect to homepage
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        
        //Else sign up new user
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup())
        { 
            $identity = User::findIdentity(['username' => $model->username]);
            if (Yii::$app->user->login($identity))
            {
                return $this->goHome();
            }
        }
        
        return $this->render('signup', [
            'model' => $model,
        ]);
    }
    
    /**
    * Logout
    */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
    * Contacts page
    */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
    * action for saving comments
    */
    public function actionComment($id)
    {
        $model = new CommentForm();

        if(Yii::$app->request->isPost)
        {
            $model->load(Yii::$app->request->post());
            if($model->saveComment($id))
            {
                Yii::$app->getSession()->setFlash('comment', 'Your comment will be added soon');
                return $this->redirect(['site/article', 'id'=>$id]);
            }            
        }
    }

    /**
    * About page
    */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
