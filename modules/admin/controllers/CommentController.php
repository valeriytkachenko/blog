<?php

namespace app\modules\admin\controllers;

use app\models\Comment;
use yii\web\Controller;
use Yii;

class CommentController extends Controller
{   
    /**
    * Redirects to login page if user is a guest
    */
    public function init()
    {       
        if(Yii::$app->admin->isGuest)
        {
            return $this->redirect(['/admin/default/login'])->send();
        }
    }
    
    public function actionIndex()
    {
        $comments = Comment::find()->orderBy('id desc')->all();

        return $this->render('index', ['comments' => $comments]);
    }

    public function actionDelete($id)
    {
        $comment = Comment::findOne($id);
        if($comment->delete())
        {
            return $this->redirect(['comment/index']);
        }
    }

    /**
     * Allows comment to display
     */
    public function actionAllow($id)
    {
        $comment = Comment::findOne($id);
        if($comment->allow())
        {
            return $this->redirect(['index']);
        }
    }
    
    /**
    * Disallows comment to display
    */
    public function actionDisallow($id)
    {
        $comment = Comment::findOne($id);
        if($comment->disallow())
        {
            return $this->redirect(['index']);
        }
    }
}