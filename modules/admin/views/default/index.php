<div class="admin-default-index">
    <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h3>Articles</h3>
                    <br>
                </div>
                <div class="icon">
                    <i class="fa fa-book"></i>
                </div>
                <a href="<?=Yii::$app->homeUrl?>admin/article" class="small-box-footer">
                go to <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>Comments</h3>
                    <br>
                </div>
                <div class="icon">
                    <i class="fa fa-comments"></i>
                </div>
                <a href="<?=Yii::$app->homeUrl?>admin/comment" class="small-box-footer">
                go to <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->

      <!-- =========================================================== -->
</div>
