<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\LinkPager;
$this->title = 'Blog';
?>
<div class="site-index">
    <div class="body-content">

        <?php  foreach ($articles as $article):?>
            <div class="row center-block">
                <div class="text-center col-md-6 col-md-offset-3 ">
                    <h2><?=$article['title']?></h2>
                    <a href="article/<?=$article->id?>">
                        <img class="img-responsive thumbnail img-center" src="uploads/images/<?=$article->image?>">
                    </a> 
                    <p style="text-align: justify;"><?=$article['description']?></p>
                    
                    <span class="badge label-default pull-left">Posted <?=$article['date']?></span>
                    <p><a class="btn badge label-success pull-right" href="article/<?=$article->id?>">Show More &raquo;</a></p>

                </div>
            </div>
        <?php endforeach; ?>
        <div class="text-center">
            <?php echo LinkPager::widget(['pagination' => $pages, ]);?>
        </div>
    </div>
</div>
