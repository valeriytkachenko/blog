<?php

/* @var $this yii\web\View */
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use app\widgets\CommentsWidget;

$this->title = $article->title;
?>
<div class="site-index">
    <div class="body-content">
        
            <div class="row center-block">
                <div class="text-center col-md-8 col-md-offset-2">
                    <h2><?=$article->title?></h2>
                    <img class="img-responsive thumbnail img-center" 
                    src="<?=Yii::$app->homeUrl . 'uploads/images/' . $article->image?>">
                    <div>
                    <?=$article->content?>
                    </div>
                    
                    <span class="badge label-default pull-left">Posted <?=$article->date?></span>
                    <p><a class="btn badge label-success pull-right" href="<?=Yii::$app->homeUrl?>">&laquo; Back</a></p>
                </div>
            </div>
            <br>
            <br>

            <?= CommentsWidget::widget([
                'article' => $article, 
                'comments' => $comments,
                'commentForm' => $commentForm,
            ]);?>
            
        </div>
    </div>
</div>
